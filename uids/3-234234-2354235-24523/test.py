# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Циклы в Python 
# Task 3 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/4fdc51de-5615-472e-b31a-3c7ece22b3f0/courses/b16b83bd-3b30-4820-a730-a427a9e6016d/topics/8d288df4-b9e2-43d1-942f-3fd5e33d8c04/lessons/9de3af3f-e225-4d2d-bd6e-e063b496572b/theory>
import ast

_test.var('payers')

has_for = _test.node(ast.For).exists
for_over_payers = _test.node(ast.For, iter__id='payers').exists
has_print_in_for = _test.node(ast.For).node(ast.Call,func__id='print').exists

assert has_for, 'Пожалуйста, напишите цикл `for`.'
assert for_over_payers, "Научите цикл `for` перебирать значения списка `payers`."
assert has_print_in_for, 'Вызовите функцию `print` в теле цикла.'

_test.output()