import pandas
data = pandas.read_csv('app_stats.csv')

payments = list(data['payments'])
installs = list(data['installs'])

campaign_weeks = [7, 9, 13, 15, 17, 19, 29, 31, 33, 45]

installs_from_ads = []
payments_from_ads = []

for week_number in campaign_weeks:
    installs_from_ads.append(installs[week_number] - installs[week_number - 1])
    payments_from_ads.append(payments[week_number] - payments[week_number - 1])
    
print(payments_from_ads)