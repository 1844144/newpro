import matplotlib # служебная строка
matplotlib.rcParams['figure.figsize'] = [15, 7] # служебная строка

import pandas
data = pandas.read_csv('app_stats.csv')
import seaborn

payments = list(data['payments'])
installs = list(data['installs'])

campaign_weeks = [7, 9, 13, 15, 17, 19, 29, 31, 33, 45]

installs_from_ads = []
payments_from_ads = []

for week_number in campaign_weeks:
    installs_from_ads.append(installs[week_number] - installs[week_number - 1])
    payments_from_ads.append(payments[week_number] - payments[week_number - 1])

conversions_from_ads = []

for index in range(len(installs_from_ads)):
    conversions_from_ads.append(payments_from_ads[index] / installs_from_ads[index])

ads_install_average_profit = []

for conversion in conversions_from_ads:
    ads_install_average_profit.append(conversion * 600)

seaborn.barplot(campaign_weeks, ads_install_average_profit)