# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Правильная конверсия
# Task 5 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/05f73a77-9e9f-4f51-8168-9432544126c1/courses/5e48d16a-79fe-4b13-a715-317a3be19762/topics/c0e11a8a-924b-4d79-8151-2a0902d45a65/lessons/77abe7d8-15b8-48d8-9081-3ff212ea0164/tasks/d19918eb-b491-4fee-a46f-8c02926af136/test>

# To be able to open file locally
def _user_precode():
    import os as _os
    _os.chdir('/datasets/')

_test.imports('pandas','seaborn')
_test.call('read_csv')
_test.var('data')

_test.var('conversions_from_ads')
_test.var('ads_install_average_profit')
_test.call('barplot', args=[campaign_weeks, ads_install_average_profit])