numbers = [277, 389, 560, 741, 150, 40, 1598]

product =  1  # задайте исходное значение переменной (англ. product, «произведение»)

# напишите цикл, который поочерёдно умножит product на значения из списка

for number in numbers:
    product *= number

print(product)