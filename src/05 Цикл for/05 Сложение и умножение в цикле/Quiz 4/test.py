# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Сложение и умножение в цикле
# Task 1 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/4fdc51de-5615-472e-b31a-3c7ece22b3f0/courses/b16b83bd-3b30-4820-a730-a427a9e6016d/topics/8d288df4-b9e2-43d1-942f-3fd5e33d8c04/lessons/f9885b5a-d67a-4b31-984f-2116c06e26bf/theory>
import ast

_test.var('numbers')

has_for = _test.node(ast.For).exists
prod_in_for = _test.node(ast.For).node(ast.BinOp, op__class=ast.Mult).exists
prodeq_in_for = _test.node(ast.For).node(ast.AugAssign, op__class=ast.Mult).exists

assert has_for, 'Добавьте цикл `for`.'
assert prod_in_for or prodeq_in_for, 'Для умножения используйте оператор `*` или `*=`.'

_test.output()