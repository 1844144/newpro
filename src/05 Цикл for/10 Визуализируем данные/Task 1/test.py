# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Визуализируем данные
# Task 1 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/05f73a77-9e9f-4f51-8168-9432544126c1/courses/5e48d16a-79fe-4b13-a715-317a3be19762/topics/c0e11a8a-924b-4d79-8151-2a0902d45a65/lessons/84182d49-8499-4568-bd6d-6d71420162f8/tasks/394f694b-ff2a-42dc-8121-e2153b628372/test>

# To be able to open file locally
def _user_precode():
    import os as _os
    _os.chdir('/datasets/')

_test.imports('pandas','seaborn')
_test.call('read_csv')
_test.var('data')

assert not _test.if_call('barplot', args=[data['installs'], data['week_number']]), 'При вызове функции порядок аргументов имеет значение. Поменяйте местами аргументы, чтобы отложить на оси X номера недель, а на оси Y — число установок.'
_test.call('barplot', args=[data['week_number'], data['installs']])