# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Визуализируем данные
# Task 5 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/05f73a77-9e9f-4f51-8168-9432544126c1/courses/5e48d16a-79fe-4b13-a715-317a3be19762/topics/c0e11a8a-924b-4d79-8151-2a0902d45a65/lessons/84182d49-8499-4568-bd6d-6d71420162f8/tasks/683b0c71-820b-4759-937d-7185120ace2d/test>

# To be able to open file locally
def _user_precode():
    import os as _os
    _os.chdir('/datasets/')

_test.imports('pandas','seaborn')
_test.call('read_csv')
_test.var('data')

import ast 
assert _test.node(ast.For).exists or _test.node(ast.comprehension).exists, "Используйте цикл `for`."

_test.var('conversions')
assert not _test.if_call('barplot', args=[conversions, data['week_number']]
    ), "Оси перепутались местами. В скобках `barplot()` укажите сначала `data['week_number']`, а затем `conversions`." 
_test.call('barplot', args=[data['week_number'], conversions])