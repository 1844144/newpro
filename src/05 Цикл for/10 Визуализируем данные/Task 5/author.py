import matplotlib # служебная строка
matplotlib.rcParams['figure.figsize'] = [15, 7] # служебная строка

import pandas
data = pandas.read_csv('app_stats.csv')

payments = list(data['payments'])
installs = list(data['installs'])

conversions = []

for index in range(len(payments)):
    conversions.append(payments[index] / installs[index])

import seaborn
seaborn.barplot(data['week_number'], conversions)