import matplotlib # служебная строка
matplotlib.rcParams['figure.figsize'] = [15, 7] # служебная строка

import pandas
data = pandas.read_csv('app_stats.csv')

import seaborn
seaborn.barplot(data['week_number'], data['payments'])