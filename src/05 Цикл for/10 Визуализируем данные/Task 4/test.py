# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Визуализируем данные
# Task 4 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/05f73a77-9e9f-4f51-8168-9432544126c1/courses/5e48d16a-79fe-4b13-a715-317a3be19762/topics/c0e11a8a-924b-4d79-8151-2a0902d45a65/lessons/84182d49-8499-4568-bd6d-6d71420162f8/tasks/0bc5dbdf-5b96-439a-ad72-053b1c8aee7c/test>

# To be able to open file locally
def _user_precode():
    import os as _os
    _os.chdir('/datasets/')

_test.imports('pandas')
_test.call('read_csv')
_test.var('data')

import ast 
assert _test.node(ast.For).exists or _test.node(ast.comprehension).exists, "Используйте цикл `for`."

_test.var('conversions')