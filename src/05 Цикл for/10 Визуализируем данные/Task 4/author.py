import pandas
data = pandas.read_csv('app_stats.csv')

payments = list(data['payments'])
installs = list(data['installs'])

conversions = []

for index in range(len(payments)):
    conversions.append(payments[index] / installs[index])

print(conversions)