# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Подсчёт суммы в цикле
# Task 2 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/05f73a77-9e9f-4f51-8168-9432544126c1/courses/5e48d16a-79fe-4b13-a715-317a3be19762/topics/c0e11a8a-924b-4d79-8151-2a0902d45a65/lessons/ec2c98b7-db2c-41b2-aa14-094b2a27253e/tasks/dd36f311-31fd-4112-9615-9898754be150/test>
import ast

_test.var('numbers')

has_for = _test.node(ast.For).exists

assert has_for, 'Кажется, вы не добавили цикл `for` :('

_test.output()