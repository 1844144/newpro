import matplotlib  # служебная строка
matplotlib.rcParams["figure.figsize"] = [7, 10]  # служебная строка

# импортируйте библиотеку pandas
import pandas
# прочитайте файл и сохраните его в переменной data
data = pandas.read_csv('app_stats.csv')
# импортируйте библиотеку seaborn
import seaborn
# постройте хитмэп
seaborn.heatmap(data)