# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Столбчатая диаграмма
# Task 1 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/4fdc51de-5615-472e-b31a-3c7ece22b3f0/courses/b16b83bd-3b30-4820-a730-a427a9e6016d/topics/8d288df4-b9e2-43d1-942f-3fd5e33d8c04/lessons/c372f7bb-014a-4e9e-9c77-e731c95f1a1a/theory>

_test.imports('pandas')
_test.call('read_csv', args=['app_stats.csv'],
    m='Аргументом должно быть имя файла')
_test.var('data')

_test.imports('seaborn')
_test.call('heatmap', args=[data])