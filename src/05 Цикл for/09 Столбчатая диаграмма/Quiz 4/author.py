import matplotlib # служебная строка
matplotlib.rcParams['figure.figsize'] = [15, 7] # служебная строка

import seaborn 

top_games = ['Counter-Strike: Global Offensive',
             'Dota 2', 
             'Team Fortress 2',
             "PLAYERUNKNOWN'S BATTLEGROUNDS",
             "Garry's Mod",
             'Grand Theft Auto V',
             'PAYDAY 2',
             'Unturned',
             'Terraria',
             'Left 4 Dead 2']

positive_reviews = [2644404, 863507, 515879, 496184, 363721,
                    329061, 308657, 292574, 255600, 251789]

seaborn.barplot(positive_reviews, top_games)