# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Лаборатория циклов
# Task 7 <https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/4fdc51de-5615-472e-b31a-3c7ece22b3f0/courses/b16b83bd-3b30-4820-a730-a427a9e6016d/topics/8d288df4-b9e2-43d1-942f-3fd5e33d8c04/lessons/a58ce78f-42ee-48ec-8dcf-4e008fae92c6/theory>


import ast
assert _test.node(ast.For).exists, 'Используйте цикл `for`.'
assert _test.node(ast.For).node(ast.Add).exists, 'В цикле используйте оператор сложения: `+` или `+=`.'
_test.call('print')
_test.output()