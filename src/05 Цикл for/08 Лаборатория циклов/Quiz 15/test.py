# <!subteam^S01506H0MEX> Аналитик 2.0 Beta / Основы Python и анализа данных 2.0 beta / Цикл for / Лаборатория циклов
# Task 15 <https://https://praktikum-admin.prestable.yandex-team.ru/faculties/d5b98ce5-3d91-47eb-ab9d-4df2bd9f465d/professions/4fdc51de-5615-472e-b31a-3c7ece22b3f0/courses/b16b83bd-3b30-4820-a730-a427a9e6016d/topics/8d288df4-b9e2-43d1-942f-3fd5e33d8c04/lessons/a58ce78f-42ee-48ec-8dcf-4e008fae92c6/theory>


_test.var('purchase_amount','payers', m='Пожалуйста, не изменяйте переменную.')

# check that there was operation * and + 
import ast
assert _test.node(ast.For).exists, 'Используйте цикл `for` для вычисления дохода за каждый месяц.'
assert _test.node(ast.Mult).exists, 'Для вычисления дохода за каждый месяц используйте умножение `*`.'

assert _test.node(ast.Add).exists or _test.if_call('sum'), 'Для вычисления суммы доходов используйте функцию `sum` или операцию сложения: `+` или `+=`.'

# and check result
_test.output()